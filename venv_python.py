#!/usr/bin/env python3

# This is a wrapper script which is used to run all python scripts in venv

import subprocess
import sys

def main():
  return subprocess.call(["python3"] + sys.argv[1:])

if __name__ == '__main__':
  sys.exit(bootstrap.RunInVenvIfNecessary(main))
