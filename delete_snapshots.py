import collections
import json
import os
import requests
from git import Repo

def deleteSnapshots():
  repo = Repo('.', search_parent_directories=True)
  ref_develop = repo.commit('HEAD')
  changelist = ref_develop.diff()

  cmd2 = 'git config --global user.email "gitlab-runner"'
  cmd3 = 'git config --global user.name "gitlab"'
  bar = 'git commit -a -m \"message\"'
  cmd4 = 'git push origin main'
  
  repo.git.execute(f"{cmd2}", shell=True, with_stdout=True)
  repo.git.execute(f"{cmd3}", shell=True, with_stdout=True)
  repo.git.execute(f"{bar}", shell=True, with_stdout=True)
  repo.git.execute(f"{cmd4}", shell=True, with_stdout=True)

  changed_paths = set()
  for item in changelist:
    changed_paths.add(item.a_path)
    changed_paths.add(item.b_path)
    print("a path -> " + str(item.a_path))

  PACKAGES_URL = "https://gitlab.com/api/v4/groups/52131810/packages"
  BRANCHES_URL = "https://gitlab.com/api/v4/projects/35448499/repository/branches"
  headers = { 'PRIVATE-TOKEN': os.environ.get('PRIVATE_TOKEN') }
  

  print('--GET BRANCHES STEP--')
  branchesResponse = requests.get(url = BRANCHES_URL, headers = headers).json()
  branches = []
  for branch in branchesResponse:
    if branch['name'] != 'main' and branch['name'] != 'develop':
      print("BRANCH -> " + str(branch['name']))
      branches.append(branch['name'])
  if len(branches) == 0:
    print('No active branches')


  print('--GET PACKAGES STEP--')
  snapshots = {}
  packagesResponse = requests.get(url = PACKAGES_URL, headers = headers)
  if packagesResponse.status_code >= 200 and packagesResponse.status_code < 300:
    print('Response Ok')
    data =  packagesResponse.json()
    for item in data:
      print('VERSION -> ' + str(item['version']))
      snapshots[item['version']] = item['_links']['delete_api_path']
  else:
    print('Response Failed with code: ' + str(packagesResponse.status_code))


  print('--SEARCH STEP--')
  forDelete = []
  k = 0
  print('Search for builds from deleted branches')
  if len(branches) != 0:
    for version in snapshots:
      for branch in branches:
        if branch in version:
          k += 1
      if k == 0 and "PULSEMOB" in version:
        forDelete.append(snapshots[version])
      k = 0     
  else:
    for version in snapshots:
      if "PULSEMOB" in version:
        forDelete.append(snapshots[version])


  print('--DELETE STEP--')
  for item in forDelete:
    print('will be deleted -> ' + str(item))

  if len(forDelete) != 0:
    for item in forDelete:
      packagesResponse = requests.delete(url = item, headers = headers)
      print('status code -> ' + str(packagesResponse.status_code))
  else:
    print('No versions with PULSEMOB suffix')


if __name__ == '__main__':
  deleteSnapshots()
