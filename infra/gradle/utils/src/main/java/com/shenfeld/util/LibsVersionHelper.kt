package com.shenfeld.util

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.io.FileInputStream
import java.util.*

open class LibsVersionHelper : DefaultTask() {

    companion object {
        private const val LIBS_VERSION_NAME = "libsVersionName"
        private const val LIBS_NEXT_VERSION = "libsNextVersion"
        private const val SKIP = "skip"
        private const val COMMENT =
            "Properties with prefix \"global.\" will be exported to all projects.\n" +
                    "i.e. global.SMTH=1 will be accessible from any project as property SMTH."
    }

    @Internal
    lateinit var omniusProperties: File


    @TaskAction
    fun updateLibsVersion() {
        if (!omniusProperties.canRead()) {
            project.logger.lifecycle("cannot read the file ${omniusProperties.absolutePath}")
            return
        }

        val omniusProps = Properties()
        omniusProps.load(FileInputStream(omniusProperties))
        val libsNextVersion = omniusProps.getProperty(LIBS_NEXT_VERSION)
        val currentVersion = omniusProps.getProperty(LIBS_VERSION_NAME)
        project.logger.lifecycle("current version libs -> $currentVersion")

        if (libsNextVersion != SKIP) {
            omniusProps.setProperty(LIBS_NEXT_VERSION, SKIP)
            omniusProps.setProperty(LIBS_VERSION_NAME, libsNextVersion)
            project.logger.lifecycle("new version -> $libsNextVersion")
        } else {
            val normalVersion = currentVersion.substringBefore("-")
            project.logger.lifecycle("normal version -> $normalVersion")

            val versionPostfix = normalVersion.substringAfterLast(".").toInt() + 1
            project.logger.lifecycle("incremented version postfix -> $versionPostfix")

            val newVersion = normalVersion.substringBeforeLast(".") + ".${versionPostfix}"
            project.logger.lifecycle("new version -> $newVersion")

            omniusProps.setProperty(LIBS_VERSION_NAME, newVersion)
        }

        omniusProps.store(omniusProperties.writer(), COMMENT)
        project.logger.lifecycle("updateLibsVersion SUCCESSFUL")
    }
}
