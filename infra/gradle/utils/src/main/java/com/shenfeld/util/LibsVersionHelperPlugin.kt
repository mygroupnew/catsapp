package com.shenfeld.util

import org.gradle.api.Plugin
import org.gradle.api.Project

class LibsVersionHelperPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        project.tasks.register("updateLibsVersion", LibsVersionHelper::class.java)
    }
}