package com.shenfeld.catsapp.app.favorites

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.GridLayoutManager
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.app.MainActivity
import com.shenfeld.catsapp.app.favorites.adapter.FavAdapter
import com.shenfeld.catsapp.base.BaseFragment
import com.shenfeld.catsapp.databinding.FragmentFavoritesBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavoritesFragment : BaseFragment<FavoritesViewModel, FragmentFavoritesBinding>() {
    override val layoutId: Int = R.layout.fragment_favorites
    override val viewModel: FavoritesViewModel by viewModel()
    override fun getViewBinding(): FragmentFavoritesBinding = FragmentFavoritesBinding.inflate(layoutInflater)

    private lateinit var catAdapter: FavAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onViewCreated()
        setupRV()
        setupObservers()
        setupListeners()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    private fun setupRV() {
        catAdapter = FavAdapter(
            onItemClicked = { cat ->
                navController.navigate(
                    FavoritesFragmentDirections.actionFavoritesFragmentToCatDetailedFragment(
                        cat = cat
                    )
                )
            },
            deleteFromFavorite = { cat, pos ->
                viewModel.deleteFromFavorite(cat = cat)
                catAdapter.remove(pos)
                (activity as? MainActivity)?.setupFavoritesBadge(isAdded = false)
            },
            onSuccessFilter = {
                binding.tvError.isVisible = false
            },
            onNothingFound = {
                binding.tvError.isVisible = true
            }

        )

        binding.rvCats.apply {
            layoutManager = GridLayoutManager(context, 2)
            setHasFixedSize(true)
            adapter = catAdapter
        }
    }

    private fun setupObservers() {
        viewModel.catsLiveData.observe(viewLifecycleOwner) {
            catAdapter.addAll(it)
            catAdapter.originalList = it
        }
    }

    private fun setupListeners() {
        binding.etSearch.doAfterTextChanged { query ->
            query ?: return@doAfterTextChanged
            catAdapter.search(query = query.toString())
        }
    }
}