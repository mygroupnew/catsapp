package com.shenfeld.catsapp.app.main

import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.filter
import com.shenfeld.catsapp.base.BaseViewModel
import com.shenfeld.catsapp.data.repositories.AppRepository
import com.shenfeld.catsapp.data.repositories.CatsSource
import com.shenfeld.catsapp.data.repositories.models.Cat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class MainViewModel(
    private val appRepository: AppRepository,
) : BaseViewModel() {
    val catsFlow = Pager(PagingConfig(pageSize = 10, prefetchDistance = 2)) {
        CatsSource(appRepository = appRepository, fromDatabase = false)
    }.flow.cachedIn(viewModelScope)

    val hairlessFlow = catsFlow.map { data ->
        data.filter { cat ->
            cat.hairless == 1
        }
    }.flowOn(Dispatchers.Default).cachedIn(viewModelScope)

    val withHair = catsFlow.map { data ->
        data.filter { cat ->
            cat.hairless == 0
        }
    }.flowOn(Dispatchers.Default).cachedIn(viewModelScope)

    val mostEnergy = catsFlow.map { data ->
        data.filter { cat ->
            cat.energyLevel >= 4
        }
    }.flowOn(Dispatchers.Default).cachedIn(viewModelScope)

    val hypoallergenic = catsFlow.map { data ->
        data.filter { cat ->
            cat.hypoallergenic == 1
        }
    }

    fun addToFavorite(cat: Cat) {
        viewModelScope.launch {
            appRepository.addToFavorites(cat)
        }
    }

    fun deleteFromFavorite(cat: Cat) {
        viewModelScope.launch {
            appRepository.deleteFromFavorites(cat = cat)
        }
    }
}