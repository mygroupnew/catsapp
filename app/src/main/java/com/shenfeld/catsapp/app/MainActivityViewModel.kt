package com.shenfeld.catsapp.app

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.shenfeld.catsapp.base.BaseViewModel
import com.shenfeld.catsapp.data.repositories.AppRepository
import kotlinx.coroutines.launch

class MainActivityViewModel(private val appRepository: AppRepository) : BaseViewModel() {
    private val _lastFavBadgeCountLiveData = MutableLiveData<Int>()
    val lastFavBadgeCountLiveData: LiveData<Int> = _lastFavBadgeCountLiveData

    private val _lastDownloadsBadgeCountLiveData = MutableLiveData<Int>()
    val lastDownloadsBadgeCountLiveData: LiveData<Int> = _lastDownloadsBadgeCountLiveData

    fun onCreate() {
        viewModelScope.launch {
            _lastFavBadgeCountLiveData.value = appRepository.getLastSavedBadgeCount()
            _lastDownloadsBadgeCountLiveData.value = appRepository.getLastDownloadsBadgeCount()
        }
    }

    fun saveFavBadgeCount(badgeCount: Int) {
        viewModelScope.launch {
            appRepository.saveLastFavBadgeCount(count = badgeCount)
        }
    }

    fun saveDownloadsBadgeCount(badgeCount: Int) {
        viewModelScope.launch {
            appRepository.saveLastDownloadsBadgeCount(count = badgeCount)
        }
    }
}