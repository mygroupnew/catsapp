package com.shenfeld.catsapp.app.favorites.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.base.BaseAdapter
import com.shenfeld.catsapp.data.repositories.models.Cat
import com.shenfeld.catsapp.databinding.ItemCatBinding
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class FavAdapter(
    private val onItemClicked: (Cat) -> Unit,
    private val deleteFromFavorite: (Cat, Int) -> Unit,
    private val onSuccessFilter: () -> Unit,
    private val onNothingFound: (() -> Unit)? = null,
) : BaseAdapter<Cat, FavAdapter.FavVH>(), Filterable {

    lateinit var originalList: List<Cat>

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FavVH {
        return FavVH(
            ItemCatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(
        holder: FavVH,
        position: Int
    ) {
        getItem(position = position)?.let { holder.bind(cat = it) }
    }

    inner class FavVH(private val binding: ItemCatBinding) :
        RecyclerView.ViewHolder(binding.root), Bindable<Cat> {

        init {
            itemView.setOnClickListener {
                getItem(absoluteAdapterPosition)?.let { cat ->
                    onItemClicked.invoke(cat)
                }
            }
        }

        override fun bind(cat: Cat) {
            Picasso.get()
                .load(cat.imageUrl)
                .resizeDimen(R.dimen.cat_size_image, R.dimen.cat_size_image)
                .centerCrop()
                .placeholder(ContextCompat.getDrawable(binding.root.context, R.drawable.ic_downloaded_unselect)!!)
                .transform(RoundedCornersTransformation(8, 0))
                .into(binding.ivCat)

            binding.tvName.text = cat.name

            if (cat.inFavorites) {
                binding.ivAddToFavorite.setImageResource(R.drawable.ic_favourite_select)
            } else {
                binding.ivAddToFavorite.setImageResource(R.drawable.ic_add_to_fav_unselect)
            }

            binding.cvContainer.setOnClickListener {
                onItemClicked.invoke(cat)
            }

            binding.ivAddToFavorite.setOnClickListener {
                cat.inFavorites = false
                binding.ivAddToFavorite.setImageResource(R.drawable.ic_add_to_fav_unselect)
                deleteFromFavorite.invoke(cat, absoluteAdapterPosition)
            }
        }
    }

    fun search(
        query: String?,
    ) {
        filter.filter(query)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            private val filterResults = FilterResults()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                clear(withoutNotify = true)
                if (constraint == "" || constraint == null) {
                    addAll(items = originalList, withoutNotify = true)
                } else {
                    val searchResult = originalList.filter { it.name.contains(constraint, true) }
                    addAll(items = searchResult, withoutNotify = true)
                }
                filterResults.values = getItems()
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (getItems().isNullOrEmpty()) onNothingFound?.invoke()
                else onSuccessFilter.invoke()
                notifyDataSetChanged()
            }
        }
    }
}