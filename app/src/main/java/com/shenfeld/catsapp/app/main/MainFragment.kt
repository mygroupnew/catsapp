package com.shenfeld.catsapp.app.main

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.app.MainActivity
import com.shenfeld.catsapp.app.main.adapter.CatsAdapter
import com.shenfeld.catsapp.app.main.adapter.CatsLoadStateAdapter
import com.shenfeld.catsapp.base.BaseFragment
import com.shenfeld.catsapp.databinding.FragmentMainBinding
import com.shenfeld.catsapp.enums.FilterType
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment<MainViewModel, FragmentMainBinding>(),
    AdapterView.OnItemSelectedListener {
    override val layoutId: Int = R.layout.fragment_main
    override val viewModel: MainViewModel by viewModel()
    override fun getViewBinding(): FragmentMainBinding = FragmentMainBinding.inflate(layoutInflater)

    private lateinit var catAdapter: CatsAdapter
    private lateinit var arrSpinner: Array<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRV()
        setupObservers()
        setupListeners()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

    private fun setupRV() {
        catAdapter = CatsAdapter(
            addToFavoriteClicked = { cat ->
                viewModel.addToFavorite(cat = cat)
                (activity as? MainActivity)?.setupFavoritesBadge(isAdded = true)
            },

            deleteFromFavorite = { cat, _ ->
                viewModel.deleteFromFavorite(cat = cat)
                (activity as? MainActivity)?.setupFavoritesBadge(isAdded = false)
            },

            onItemClicked = { cat ->
                navController.navigate(
                    MainFragmentDirections.actionMainFragmentToCatDetailedFragment(
                        cat = cat
                    )
                )
            }
        )

        binding.rvCats.apply {
            layoutManager = GridLayoutManager(context, 2)
            setHasFixedSize(true)
            adapter = catAdapter.withLoadStateFooter(footer = CatsLoadStateAdapter {
                catAdapter.retry()
            })
        }

        catAdapter.addLoadStateListener { loadState ->
            when (loadState.refresh) {
                is LoadState.Loading -> {
                    binding.btnRetry.isVisible = false
                    binding.progressBar.isVisible = true
                }

                is LoadState.NotLoading -> {
                    binding.btnRetry.isVisible = false
                    binding.progressBar.isVisible = false
                }

                else -> {
                    val errorState = when {
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.refresh is LoadState.Error -> {
                            binding.btnRetry.isVisible = true
                            loadState.refresh as LoadState.Error
                        }
                        else -> null
                    }
                    errorState?.let {
                        Toast.makeText(context, it.error.message, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun setupObservers() {
        lifecycleScope.launch {
            viewModel.catsFlow.collect {
                catAdapter.submitData(it)
            }
        }
    }

    private fun setupListeners() {
        arrSpinner = resources.getStringArray(R.array.planets_array)
        val spinner = binding.spinnerFilter
        ArrayAdapter.createFromResource(
            context!!,
            R.array.planets_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        when (arrSpinner[position]) {
            FilterType.SELECT_FILTER.value -> {
            }

            FilterType.ALL.value -> {
                lifecycleScope.launch {
                    viewModel.catsFlow.collect {
                        catAdapter.submitData(it)
                    }
                }
            }

            FilterType.HAIRLESS.value -> {
                lifecycleScope.launch {
                    viewModel.hairlessFlow.collect {
                        catAdapter.submitData(it)
                    }
                }
            }

            FilterType.WITH_HAIR.value -> {
                lifecycleScope.launch {
                    viewModel.withHair.collect {
                        catAdapter.submitData(it)
                    }
                }
            }

            FilterType.HYPOALLERGENIC.value -> {
                lifecycleScope.launch {
                    viewModel.hypoallergenic.collect {
                        catAdapter.submitData(it)
                    }
                }
            }

            FilterType.MOST_ENERGETIC.value -> {
                lifecycleScope.launch {
                    viewModel.mostEnergy.collect {
                        catAdapter.submitData(it)
                    }
                }
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        /** no-op **/
    }
}