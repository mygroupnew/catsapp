package com.shenfeld.catsapp.app.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.data.repositories.models.Cat
import com.shenfeld.catsapp.databinding.ItemCatBinding
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class CatsAdapter(
    private val addToFavoriteClicked: ((Cat) -> Unit)? = null,
    private val deleteFromFavorite: (Cat, Int) -> Unit,
    private val onItemClicked: (Cat) -> Unit,
) : PagingDataAdapter<Cat, RecyclerView.ViewHolder>(
    diffCallback = CatModelComparator
) {
    companion object {
        private val CatModelComparator = object : DiffUtil.ItemCallback<Cat>() {
            override fun areItemsTheSame(oldItem: Cat, newItem: Cat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Cat, newItem: Cat): Boolean =
                oldItem == newItem
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position = position)?.let { cat ->
            val catHolder = holder as CatVH

            Picasso.get()
                .load(cat.imageUrl)
                .resizeDimen(R.dimen.cat_size_image_width, R.dimen.cat_size_image)
                .centerCrop()
                .placeholder(ContextCompat.getDrawable(holder.bindingCat.root.context, R.drawable.ic_downloaded_unselect)!!)
                .transform(RoundedCornersTransformation(8, 0))
                .into(holder.bindingCat.ivCat)

            catHolder.bindingCat.tvName.text = cat.name

            if (cat.inFavorites) {
                catHolder.bindingCat.ivAddToFavorite.setImageResource(R.drawable.ic_favourite_select)
            } else {
                catHolder.bindingCat.ivAddToFavorite.setImageResource(R.drawable.ic_add_to_fav_unselect)
            }

            catHolder.bindingCat.cvContainer.setOnClickListener {
                onItemClicked.invoke(cat)
            }

            catHolder.bindingCat.ivAddToFavorite.setOnClickListener {
                if (cat.inFavorites) {
                    cat.inFavorites = false
                    catHolder.bindingCat.ivAddToFavorite.setImageResource(R.drawable.ic_add_to_fav_unselect)
                    deleteFromFavorite.invoke(cat, catHolder.absoluteAdapterPosition)
                } else {
                    cat.inFavorites = true
                    catHolder.bindingCat.ivAddToFavorite.setImageResource(R.drawable.ic_favourite_select)
                    addToFavoriteClicked?.invoke(cat)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CatVH(ItemCatBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    inner class CatVH(val bindingCat: ItemCatBinding) : RecyclerView.ViewHolder(bindingCat.root)
}