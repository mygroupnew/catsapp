package com.shenfeld.catsapp.app.downloads.adapter

import android.net.Uri
import com.shenfeld.catsapp.base.RvModelBase

data class ImageItem(
    val uri: Uri,
) : RvModelBase(Type.IMAGE.ordinal) {
    enum class Type {
        IMAGE
    }
}
