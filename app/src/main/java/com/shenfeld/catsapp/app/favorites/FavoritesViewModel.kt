package com.shenfeld.catsapp.app.favorites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.shenfeld.catsapp.base.BaseViewModel
import com.shenfeld.catsapp.data.repositories.AppRepository
import com.shenfeld.catsapp.data.repositories.models.Cat
import kotlinx.coroutines.launch

class FavoritesViewModel(
    private val appRepository: AppRepository,
) : BaseViewModel() {
    private val _catsLiveData = MutableLiveData<List<Cat>>()
    val catsLiveData: LiveData<List<Cat>> = _catsLiveData

    private var currList = mutableListOf<Cat>()

    fun onViewCreated() {
        viewModelScope.launch {
            currList.clear()
            currList.addAll(appRepository.getAllFavorites())

            _catsLiveData.value = currList
        }
    }

    fun deleteFromFavorite(cat: Cat) {
        viewModelScope.launch {
            appRepository.deleteFromFavorites(cat = cat)
        }
    }
}