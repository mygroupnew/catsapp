package com.shenfeld.catsapp

import java.util.concurrent.TimeUnit

object Constants {
    const val X_API_KEY = "7de05a1a-f691-4784-8019-1658230eaaf3"
    const val BASE_URL = "https://api.thecatapi.com/v1/"
    const val API_TIMEOUT_VALUE = 100L
    val API_TIMEOUT_UNIT = TimeUnit.SECONDS

}