package com.shenfeld.catsapp

import android.app.Application
import com.shenfeld.catsapp.di.dataModule
import com.shenfeld.catsapp.di.repoModule
import com.shenfeld.catsapp.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(androidContext = this@App)
            modules(
                listOf(
                    viewModelsModule,
                    dataModule,
                    repoModule,
                )
            )
        }
    }
}