package com.shenfeld.catsapp.di

import com.shenfeld.catsapp.Constants
import com.shenfeld.catsapp.Constants.API_TIMEOUT_UNIT
import com.shenfeld.catsapp.Constants.API_TIMEOUT_VALUE
import com.shenfeld.catsapp.base.LoggingInterceptor
import com.shenfeld.catsapp.data.AppInterceptor
import com.shenfeld.catsapp.data.api.CatsAPI
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val dataModule = module {
    single(named("LogcatLogger")) {
        val loggingInterceptor = HttpLoggingInterceptor(LoggingInterceptor())
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return@single loggingInterceptor
    }

    single<CatsAPI> {
        val okHttpClient = getOkHttpClientBuilder()
            .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT))
            .addInterceptor(AppInterceptor())

        val retrofit = Retrofit.Builder()
            .client(okHttpClient.build())
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return@single retrofit.create(CatsAPI::class.java)
    }
}

fun getOkHttpClientBuilder(): OkHttpClient.Builder {
    return OkHttpClient.Builder()
        .connectTimeout(API_TIMEOUT_VALUE, API_TIMEOUT_UNIT)
        .readTimeout(API_TIMEOUT_VALUE, API_TIMEOUT_UNIT)
        .writeTimeout(API_TIMEOUT_VALUE, API_TIMEOUT_UNIT)
        .callTimeout(API_TIMEOUT_VALUE, API_TIMEOUT_UNIT)
}