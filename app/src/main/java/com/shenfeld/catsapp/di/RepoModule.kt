package com.shenfeld.catsapp.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import androidx.room.RoomDatabase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.shenfeld.catsapp.data.db.AppDatabase
import com.shenfeld.catsapp.data.repositories.AppRepository
import com.shenfeld.catsapp.data.repositories.AppStorage
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val repoModule = module {
    single(named("CATS_PREFS")) { createSharedPreferences(context = androidContext(), prefName = "TIMETABLE_PREFS") }
    single { createGsonInstance() }

    single { AppRepository(appStorage = get(), catsAPI = get(), appDatabase = get()) }
    single { AppStorage(sharedPreferences = get(named("CATS_PREFS"))) }
    single {
        return@single Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java, "cat_database"
        ).build()
    }
}

private fun createSharedPreferences(context: Context, prefName: String): SharedPreferences {
    return context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
}

private fun createGsonInstance(): Gson {
    return GsonBuilder().create()
}