package com.shenfeld.catsapp.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<VM: BaseViewModel, VB: ViewBinding> : Fragment(), BackPressed {
    @get:LayoutRes
    protected abstract val layoutId: Int
    protected abstract val viewModel: VM
    private var _binding: VB? = null
    protected abstract fun getViewBinding(): VB
    protected val binding: VB
        get() = requireNotNull(_binding)
    val navController: NavController by lazy { findNavController() }


    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        init()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("KEK::", "onDestroy")
        _binding = null
    }

    private fun init() {
        _binding = getViewBinding()
    }
}
