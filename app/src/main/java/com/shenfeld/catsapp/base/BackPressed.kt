package com.shenfeld.catsapp.base

interface BackPressed {
    fun onBackPressed(): Boolean
}