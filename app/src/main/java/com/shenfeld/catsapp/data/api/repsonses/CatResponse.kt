package com.shenfeld.catsapp.data.api.repsonses

import com.google.gson.annotations.SerializedName


data class CatResponse(
    val id: String,
    val name: String,
    val description: String,
    val image: Image?,
    val temperament: String,
    val origin: String,
    @SerializedName("wikipedia_url")
    val wikiUrl: String?,
    @SerializedName("country_code")
    val countryCode: String,
    @SerializedName("energy_level")
    val energyLevel: Int,
    val grooming: Int,
    val hypoallergenic: Int,
    val hairless: Int,
) {
    data class Image(
        val id: String,
        val url: String?,
    )
}

