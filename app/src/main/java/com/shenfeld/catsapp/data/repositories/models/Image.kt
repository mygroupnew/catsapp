package com.shenfeld.catsapp.data.repositories.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.shenfeld.catsapp.base.RvModelBase
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Image(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "image") val image: String,
) : RvModelBase(ItemType.IMAGE_ITEM.ordinal), Parcelable {
    enum class ItemType {
        IMAGE_ITEM
    }
}
